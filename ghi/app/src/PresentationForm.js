import React, { useEffect, useState} from "react";

function PresentationForm () {

    const [conferences, setConferences] = useState([])

    // const [formData, setFormData] = useState({
    //     presenter_name: '',
    //     presenter_email: '',
    //     title: '',
    //     synopsis: '',
    //     conference:'',
    // })

    const [conference, setConference] = useState('');
    const [presenter_name, setPresenterName] = useState('');
    const [presenter_email, setPresenterEmail] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.conference = conference
        data.presenter_name = presenter_name
        data.presenter_email = presenter_email
        data.title = title
        data.synopsis = synopsis


        const presentationUrl = `http://localhost:8000${conference}presentations/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            const newPresentation = await response.json();
            setConference('');
            setPresenterName('');
            setPresenterEmail('');
            setTitle('');
            setSynopsis('');
    }
}
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    // const handleFormChange = (e) => {
    //     const value = e.target.value;
    //     const inputName = e.target.name;

    //     setFormData({
    //         ...formData,


    //     [inputName]: value
    //     });
    // }

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
                <input value={presenter_name} onChange={handlePresenterNameChange} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter Name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={presenter_email} onChange={handlePresenterEmailChange} placeholder="presenter_email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter Email</label>
            </div>
            <div className="form-floating mb-3">
                <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" name="Title" id="Title" className="form-control"/>
                <label htmlFor="Title">Title</label>
            </div>
            <div className="mb-3">
                <label htmlFor="Synopsis">Synopsis</label>
                <textarea value={synopsis} onChange={handleSynopsisChange} placeholder="Synopsis" required type="textarea" name="Synopsis" id="Synopsis" className="form-control">
                </textarea>
            </div>
            <div className="mb-3">
                <select value={conference} onChange={handleConferenceChange} name="conference" id="conference" className="form-select" required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        )
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    )

}
export default PresentationForm
