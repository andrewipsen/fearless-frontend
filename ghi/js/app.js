function createCard(name, description, locName, pictureUrl, dateRange) {
    return `
        <div class="col">
            <div class="card">
                <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle mb-2 text-body-secondary">${locName}</h6>
                        <p class="card-text">${description}</p>
                    </div>
                    <div class="card-footer text-body-secondary text-muted">
                    ${dateRange}
                    </div>
                </div>
            </div>
        </div>
    `;
    }


window.addEventListener('DOMContentLoaded', async () => {



    const url = "http://localhost:8000/api/conferences";

    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Bad response')
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const locName = details.conference.location.name;

                    const dateStart = details.conference.starts;
                    const dateEnd = details.conference.ends;

                    const dateStartObj = new Date(dateStart);
                    const dateEndObj = new Date(dateEnd);


                    const formattedStartDate = dateStartObj.toLocaleDateString();
                    const formattedEndDate = dateEndObj.toLocaleDateString();


                    const dateRange = `${formattedStartDate} - ${formattedEndDate}`;

                    const html = createCard(name, description, locName, pictureUrl, dateRange);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                    console.log(details)
                }
                }



        }
    } catch (e) {
        console.log(e)
    }

});
