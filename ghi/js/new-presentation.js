window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
        }

        // Here, add the 'd-none' class to the loading icon
        // Here, remove the 'd-none' class from the select tag


        selectTag.classList.remove("d-none")

    }

});

const formTag = document.getElementById("create-presentation-form")
formTag.addEventListener('submit', async(event) => {
    event.preventDefault();
    const successTag = document.getElementById("success-message")

    try {
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const presentationUrl = `"http://localhost:8000/api/presentations/${selectTag.value}"`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        }
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation)
        }
        successTag.classList.remove("d-none")
    } catch (e) {
        console.log("error:", e)
    }

})
